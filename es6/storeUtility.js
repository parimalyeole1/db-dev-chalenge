export const addCurrancyPairToStore = (store, currencyPair) => {
  const currancyName = currencyPair.name;
  store[currancyName] = store[currancyName] || {
    current: null,
    previous: [],
    sparkDomRef: null
  };
  store[currancyName].current = currencyPair;
  const previous = store[currancyName].previous;
  previous.push({
    date: new Date(),
    sparkPoint: (currencyPair.bestBid + currencyPair.bestAsk) / 2
  });
  return store;
};

export const createStoreArr = store => {
  const storeArr = [];
  for (var key in store) {
    if (store.hasOwnProperty(key)) {
      if (store[key].current) {
        storeArr.push(store[key].current);
      }
    }
  }
  return storeArr;
};

// NOTE: depend of arr and sortby  values I should have create this function reuseble for "string", "number"
// Right now it only sort on "number" return by a[sortby] and b[sortby]
export const sortArrayBy = (arr, sortby) =>
  arr.sort((a, b) => a[sortby] - b[sortby]);
