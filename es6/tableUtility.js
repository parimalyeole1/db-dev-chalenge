export const columnTitles = [
  "name",
  "bestBid",
  "bestAsk",
  "openBid",
  "openAsk",
  "lastChangeAsk",
  "lastChangeBid",
  "graph"
];

export const creteteTableifNot = (table, colTitles) => {
  if (!table) {
    table = document.createElement("table");
    table.setAttribute("id", "table");
    table = setColTitleTable(table, colTitles);
  }
  return table;
};

export const createRowNInsertToTable = (
  store,
  table,
  currencyArr,
  titleArr,
  currentCurrancy
) => {
  const currancyName = currentCurrancy.name;
  const index = currencyArr.findIndex(o => o.name === currancyName);
  removeDomWithIdIfExist(currancyName);
  const tableRow = createRowWithIdOnTable(table, index, currancyName);

  titleArr.forEach(title => {
    const tabCell = tableRow.insertCell(-1);
    if (title == "graph") {
      // create sparkLine span
      const sparklineSpan = document.createElement("span");
      // keeping ref of sparkLine span[DOM] in store with respective currency
      store[currancyName].sparkDomRef = sparklineSpan;
      // removing SparkPoint Olderthan 30 sec and updating store for respective currency
      let previous = store[currancyName].previous;
      previous = removeSparkPointOlderthanGivenSec(previous);

      const sparkLineArr = previous.map(x => x.sparkPoint);
      Sparkline.draw(sparklineSpan, sparkLineArr);
      tabCell.appendChild(sparklineSpan);
    } else {
      tabCell.innerHTML = currentCurrancy[title] || "NA";
    }
  });
  return table;
};


//  Function make sure that it remove sparkpoint older than given time
// Note: this function purpose can be solve without recursion (may be normal sync looping best fit eg reduce, forloop with break)
export const removeSparkPointOlderthanGivenSec = (previous, timeDiffInsecond = 30) => {
  if (previous.length > 1) {
    const first = previous[0];
    const last = previous[previous.length - 1];
    const diff = (last.date - first.date) / 1000;
    if (diff >= timeDiffInsecond) {
      previous.splice(0, 1);
      removeSparkPointOlderthanGivenSec(previous);
    }
  }
  return previous;
};

function setColTitleTable(table, titleArr) {
  const tr = table.insertRow(-1);
  tr.setAttribute("id", "colTitles");
  titleArr.forEach(colTitle => {
    const th = document.createElement("th");
    th.innerHTML = colTitle.toUpperCase();
    tr.appendChild(th);
  });
  return table;
}

function removeDomWithIdIfExist(selectorId) {
  // Note : Need more specific in choosing row eg. #tableId -> #rowId
  const row = document.getElementById(selectorId);
  row && row.parentNode.removeChild(row);
}

function createRowWithIdOnTable(table, index, selectorId) {
  const tr = table.insertRow(index + 1);
  tr.setAttribute("id", selectorId);
  return tr;
}
