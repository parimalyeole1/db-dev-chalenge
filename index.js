// import { setInterval } from "../../../.cache/typescript/2.6/node_modules/@types/stompjs";

/**
 * This javascript file will constitute the entry point of your solution.
 *
 * Edit it as you need.  It currently contains things that you might find helpful to get started.
 */

// This is not really required, but means that changes to index.html will cause a reload.
require("./site/index.html");
// Apply the styles in style.css to the page.
require("./site/style.css");

// if you want to use es6, you can do something like
const columnTitles = require("./es6").columnTitles;
const creteteTableifNot = require("./es6").creteteTableifNot;
const addCurrancyPairToStore = require("./es6").addCurrancyPairToStore;
const createRowNInsertToTable = require("./es6").createRowNInsertToTable;
const removeSparkPointOlderthanGivenSec = require("./es6")
  .removeSparkPointOlderthanGivenSec;
const sortArrayBy = require("./es6").sortArrayBy;
const createStoreArr = require("./es6").createStoreArr;
// here to load the myEs6code.js file, and it will be automatically transpiled.

const exampleSparkline = document.getElementById("example-sparkline");
Sparkline.draw(exampleSparkline, [1, 12.9, 34, 4, 4.4, 67.4, 8.9, 890, 0]);

let store = {};
/*
store structure
{
  gbpusd: {
    current: {
      name: "gbpusd",
      bestBid: 1.4178968724609506,
      bestAsk: 1.4387282849504348,
      .
      .
      .
    },
    previous: [{
      date: //new Date(),
      sparkPoint: //(currencyPair.bestBid + currencyPair.bestAsk) / 2
    }...],
    sparkDomRef: domRef
  },
  gbpeur: {
    current: {
      name: "gbpeur",
      .
      .
      .
    previous: [],
    sparkDomRef: domRef
  },
  }
} */

let globalTable = null;
const sortTableBy = "lastChangeBid"
// Change this to get detailed logging from the stomp library
global.DEBUG = false;
// stom connection part
const url = "ws://localhost:8011/stomp";
const client = Stomp.client(url);

client.debug = function(msg) {
  if (global.DEBUG) {
    console.info(msg);
  }
};

client.connect({}, connectCallback, function(error) {
  alert(error.headers.message);
});

function connectCallback() {
  console.log(" stomp connected!!!");
  document.getElementById("stomp-status").innerHTML =
    "It has now successfully connected to a stomp server serving price updates for some foreign exchange currency pairs.";

  // Note: should catch interval id and remove it on route change if any..
  const intervalId = setInterval(() => {
    for (var currency in store) {
      if (store.hasOwnProperty(currency)) {
        let previous = store[currency].previous;
        previous = removeSparkPointOlderthanGivenSec(previous);
        const sparkPointArr = previous.map(x => x.sparkPoint);
        Sparkline.draw(store[currency].sparkDomRef, sparkPointArr);
      }
    }
  }, 1000);

  globalTable = creteteTableifNot(globalTable, columnTitles);
  renderTable(globalTable);
  client.subscribe("/fx/prices", fxPricesSubscribeCb);
}

function renderTable(table) {
  const divContainer = document.getElementById("solutionTable");
  divContainer.innerHTML = "";
  divContainer.appendChild(table);
}

function fxPricesSubscribeCb(msg) {
  console.info(" /fx/prices subcription return data!!");
  //NOTE: should have used try catch and handle error if any while parsing
  const currancyPair = JSON.parse(msg.body);
  store = addCurrancyPairToStore(store, currancyPair);
  const storeArr = sortArrayBy(createStoreArr(store), sortTableBy);
  createRowNInsertToTable(
    store,
    globalTable,
    storeArr,
    columnTitles,
    currancyPair
  );
}
